from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.

def index(request):
	return HttpResponse("<h1> Hola esta es mi primera app en Django</h1>")

def di_hola(request):
	return HttpResponse("Hola a todo el mundo")

def di_adios(request, nombre):
	return HttpResponse("Adios, " + nombre)

def di_numeros(request, num1, num2):
	return HttpResponse("Los números elegidos son: " + str(num1) + str(num2))
