from django.shortcuts import render, redirect
from django.template import loader
from django.http import HttpResponse, Http404
from django.views.decorators.csrf import csrf_exempt
from .models import Contenido
from django.contrib.auth import logout

# Create your views here.

form = '<form action="" method="post" class="form-example">' \
       + '<div class="form-example"><label for="contenido">Contenido: </label>' \
       + '<input type="contenido" name="contenido" id="contenido" required>' \
       + '</div>' \
       + '<div class="form-example">' \
       + '<input type="submit" value="Enviar">' \
       + '</div></form>'


def index(request):
    # Primer paso: obtener el contenido a tratar en el template
    content_list = Contenido.objects.all()
    # Segundo: from django.template import loader
    template = loader.get_template('cms_put_post/index.html')
    # 3 Crear el contexto que se enviara al template
    contexto = {
        'contenido_lista': content_list
    }

    # 4 renderizar el template y responder
    return HttpResponse(template.render(contexto, request))


@csrf_exempt
def get_resources(request, recurso):
    decoded = request.body.decode()
    respuesta = ""
    contenido = ""
    if request.method == "PUT":
        valor = decoded
        try:
            contenido = Contenido.objects.get(clave=recurso)
            contenido.valor = valor
            contenido.save()
        except Contenido.DoesNotExist:
            # para que solo añadan entradas los usuarios autenticados
            if request.user.is_authenticated:
                c = Contenido(clave=recurso, valor=valor)
                c.save()
            else:
                respuesta = 'No estas autenticado, <a href=/login>autenticate</a>'
            return HttpResponse(respuesta)
    if request.method == "POST":
        # valor = request.POST["contenido"] para usar los metodos de POST y no los de string de python
        valor = decoded.split('=')[1].replace("+", " ")
        c = Contenido(clave=recurso, valor=valor)
        c.save()
    try:
        contenido = Contenido.objects.get(clave=recurso)
    except Contenido.DoesNotExist:
        # para que solo añadan entradas los usuarios autenticados
        if request.user.is_authenticated:
            respuesta = 'El recurso pedido no existe' + form
        else:
            respuesta = 'No estas autenticado, <a href=/login>autenticate</a>'
        return HttpResponse(respuesta)

    respuesta = "El recurso pedido es: " + contenido.clave + " Cuyo valor es: " + contenido.valor + \
                " y su identificador es: " + str(contenido.id)
    return HttpResponse(respuesta)


def loggedin(request):
    if request.user.is_authenticated:
        respuesta = "El usuario " + request.user.username + " esta autenticado"
    else:
        respuesta = "No estás logueado, entra en el <a href=/login>login</a>"

    return HttpResponse(respuesta)


def logout_vista(request):
    logout(request)
    return redirect("/cms/")


def imagen(request):
    template = loader.get_template("cms_put_post/plantilla.html")
    contexto = {}
    return HttpResponse(template.render(contexto, request))
