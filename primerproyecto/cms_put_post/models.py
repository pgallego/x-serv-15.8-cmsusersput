from django.db import models

# Create your models here.
# dict de clave=recurso y valor=contenido de la pagina html


class Contenido(models.Model):
    def __str__(self):
        return str(self.id) + ": " + self.clave
    clave = models.CharField(max_length=64)
    valor = models.TextField()


class Comentario(models.Model):
    def __str__(self):
        return str(self.id) + ": " + self.titulo + " --- " + self.contenido.clave

    # Crear filtros
    def verificar(self):
        return "cuerpo" in self.comentario

    contenido = models.ForeignKey(Contenido, on_delete=models.CASCADE)
    titulo = models.CharField(max_length=128)
    comentario = models.TextField()
    fecha = models.DateTimeField()
