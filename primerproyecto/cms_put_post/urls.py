from django.urls import path

from . import views

urlpatterns = [
    path('', views.index),
    path('acceso', views.loggedin),
    path('logout', views.logout_vista),
    path('imagen', views.imagen),
    path('<str:recurso>/', views.get_resources),
    path('<str:recurso>', views.get_resources),
]